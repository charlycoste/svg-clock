# View class for indicators
class Indicator

  dataPathRegex: /M ([+-]?[0-9]+\.?[0-9]*),([+-]?[0-9]+\.?[0-9]*) A ([+-]?[0-9]+\.?[0-9]*),([+-]?[0-9]+\.?[0-9]*) [0-1] ([0-1]) [0-1] ([+-]?[0-9]+\.?[0-9]*),([+-]?[0-9]+\.?[0-9]*)/i

  constructor: (@el,@max)->
    args = @el.getAttribute('d').match @dataPathRegex

    [all,x1,y1,r,r,mode,x2,y2] = args.map (arg)-> parseFloat(arg)

    @cx = x1
    @cy = y1 + r
    @r = r
    @mode = mode

  set: (value)->
    @el.setAttribute 'd', @getPathData(value)

  getPathData: (value)->
    angle = 2 * value * Math.PI / this.max;

    r = @r;

    # Starting point is at {x1, y1}
    x1 = @cx;
    y1 = @cy - @r;

    # Ending point is at {x2,y2}
    x2 = @cx + @r * Math.sin(angle);
    y2 = @cy - @r * Math.cos(angle);

    if value > @max/2 then mode = 1 else mode = 0

    "M #{x1},#{y1} A #{r},#{r} 0 #{mode} 1 #{x2},#{y2}"

# View class for the clock
class AnalogClock
  constructor: (@doc)->

    @second = new Indicator @doc.getElementById('seconds'), 60
    @minute = new Indicator @doc.getElementById('minutes'), 60
    @hour = new Indicator @doc.getElementById('hours'), 24
    @day = new Indicator @doc.getElementById('days'), 31
    @month = new Indicator @doc.getElementById('months'), 12

  set: (date)->
    @second.set date.getSeconds()
    @minute.set date.getMinutes()
    @hour.set date.getHours()
    @day.set date.getDate()
    @month.set date.getMonth()
    return

class DigitalClock
  constructor: (@textElement)->

  pad:(num)->
    s = num.toString()

    while s.length < 2
      s = "0#{s}"

    return s

  set: (date)->

    @textElement.getElementsByClassName('seconds')[0].textContent = @pad date.getSeconds()
    @textElement.getElementsByClassName('minutes')[0].textContent = @pad date.getMinutes()
    @textElement.getElementsByClassName('hours')[0].textContent = @pad date.getHours()
    @textElement.getElementsByClassName('days')[0].textContent = @pad date.getDate()
    @textElement.getElementsByClassName('months')[0].textContent = @pad date.getMonth()+1
    return

class Application

  constructor: (@startButton, @analog, @digital)->

  run:->
    date = new Date();
    @analog.set date
    @digital.set date

  start:->
    @run()
    @execution = setInterval => @run(),
    1000
    @running = true

  stop:->
    clearInterval @execution
    @running = false

init= ->

  analog = new AnalogClock document
  digital = new DigitalClock document.getElementById 'time'
  application = new Application document.getElementById 'start-button', analog, digital

startButton = null
app = null

document.onclick =->
  if startButton
    document.documentElement.appendChild startButton
    startButton = false
  else
    startButton = document.getElementById "start-button"
    console.log startButton
    startButton.parentNode.removeChild startButton

  if app
    app.stop()
    app = null
  else
    analog = new AnalogClock document
    digital = new DigitalClock document.getElementById('time')
    app = new Application document.getElementById('start-button'), analog, digital
    app.start()
